<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Mapping shortcodes
 */
function ts_map_vc_shortcodes() {

	// Mapping shortcode Icon Box
	vc_map(
		array(
			'name'                    => __( 'Thim Icon Box', 'fastex_shortcodes' ),
			'base'                    => 'thim-icon-box',
			'category'                => __( 'Thim Shortcodes', 'fastex_shortcodes' ),
			'description'             => __( 'Display icon box with image or icon.', 'fastex_shortcodes' ),
			'controls'                => 'full',
			'show_settings_on_create' => true,
			'params'                  => array(

				array(
					'type'               => 'radioimage',
					'heading'            => __( 'Layout', 'fastex_shortcodes' ),
					'class'              => 'icon-box-layout',
					'param_name'         => 'layout',
					'admin_label'        => true,
					'options'            => array(
						'top'  => TS_URL . 'images/image-top.jpg',
						'top2' => TS_URL . 'images/icon-top.jpg',
						'left' => TS_URL . 'images/icon-left.jpg'
					),
					'layout_description' => array(
						'top'  => __( 'Image on top', 'fastex_shortcodes' ),
						'top2' => __( 'Icon on top', 'fastex_shortcodes' ),
						'left' => __( 'Icon on left', 'fastex_shortcodes' ),
					),
					'description'        => __( 'Choose the layout you want to display.', 'fastex_shortcodes' ),
				),
				// Title
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'fastex_shortcodes' ),
					'param_name'  => 'title',
					'admin_label' => true,
					'value'       => __( 'This is an icon box.', 'fastex_shortcodes' ),
					'description' => __( 'Provide the title for this icon box.', 'fastex_shortcodes' ),
				),

				//Use custom or default title?
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Use custom or default title?', 'fastex_shortcodes' ),
					'param_name'  => 'title_custom',
					'value'       => array(
						__( 'Default', 'fastex_shortcodes' ) => '',
						__( 'Custom', 'fastex_shortcodes' )  => 'custom',
					),
					'description' => __( 'If you select default you will use default title which customized in typography.', 'fastex_shortcodes' )
				),
				//Heading
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Heading element', 'fastex_shortcodes' ),
					'param_name'  => 'heading_tag',
					'value'       => array(
						'h3' => 'h3',
						'h2' => 'h2',
						'h4' => 'h4',
						'h5' => 'h5',
						'h6' => 'h6',
					),
					'description' => __( 'Choose heading type of the title.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'title_custom',
						'value'   => 'custom',
					),
				),
				//Title color
				array(
					'type'        => 'colorpicker',
					'admin_label' => true,
					'heading'     => __( 'Title color ', 'fastex_shortcodes' ),
					'param_name'  => 'title_color',
					'value'       => __( '', 'fastex_shortcodes' ),
					'description' => __( 'Select the title color.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'title_custom',
						'value'   => 'custom',
					),
				),
				//Title size
				array(
					'type'        => 'number',
					'admin_label' => true,
					'heading'     => __( 'Title size ', 'fastex_shortcodes' ),
					'param_name'  => 'title_size',
					'min'         => 0,
					'value'       => '',
					'suffix'      => 'px',
					'description' => __( 'Select the title size.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'title_custom',
						'value'   => 'custom',
					),
				),
				//Title weight
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Title weight ', 'fastex_shortcodes' ),
					'param_name'  => 'title_weight',
					'value'       => array(
						__( 'Choose the title font weight', 'fastex_shortcodes' ) => '',
						__( 'Normal', 'fastex_shortcodes' )                       => 'normal',
						__( 'Bold', 'fastex_shortcodes' )                         => 'bold',
						__( 'Bolder', 'fastex_shortcodes' )                       => 'bolder',
						__( 'Lighter', 'fastex_shortcodes' )                      => 'lighter',
					),
					'description' => __( 'Select the title weight.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'title_custom',
						'value'   => 'custom',
					),
				),
				//Title style
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Title style ', 'fastex_shortcodes' ),
					'param_name'  => 'title_style',
					'value'       => array(
						__( 'Choose the title font style', 'fastex_shortcodes' ) => '',
						__( 'Italic', 'fastex_shortcodes' )                      => 'italic',
						__( 'Oblique', 'fastex_shortcodes' )                     => 'oblique',
						__( 'Initial', 'fastex_shortcodes' )                     => 'initial',
						__( 'Inherit', 'fastex_shortcodes' )                     => 'inherit',
					),
					'description' => __( 'Select the title style.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'title_custom',
						'value'   => 'custom',
					),
				),
				// Description
				array(
					'type'        => 'textfield',
					'admin_label' => true,
					'heading'     => __( 'Description', 'fastex_shortcodes' ),
					'param_name'  => 'description',
					'value'       => __( '', 'fastex_shortcodes' ),
					'description' => __( 'Provide the description for this icon box.', 'fastex_shortcodes' )
				),
				//Use custom or default description ?
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Use custom or default description?', 'fastex_shortcodes' ),
					'param_name'  => 'description_custom',
					'value'       => array(
						__( 'Default', 'fastex_shortcodes' ) => '',
						__( 'Custom', 'fastex_shortcodes' )  => 'custom',
					),
					'description' => __( 'If you select default you will use default description which customized in typography.', 'fastex_shortcodes' )
				),

				//Description color
				array(
					'type'        => 'colorpicker',
					'admin_label' => true,
					'heading'     => __( 'Description color ', 'fastex_shortcodes' ),
					'param_name'  => 'description_color',
					'value'       => __( '', 'fastex_shortcodes' ),
					'description' => __( 'Select the description color.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'description_custom',
						'value'   => 'custom',
					),
				),
				//Description size
				array(
					'type'        => 'number',
					'admin_label' => true,
					'heading'     => __( 'Description size ', 'fastex_shortcodes' ),
					'param_name'  => 'description_size',
					'min'         => 0,
					'value'       => '',
					'suffix'      => 'px',
					'description' => __( 'Select the description size.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'description_custom',
						'value'   => 'custom',
					),
				),
				//Description weight
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Description weight ', 'fastex_shortcodes' ),
					'param_name'  => 'description_weight',
					'value'       => array(
						__( 'Choose the description font weight', 'fastex_shortcodes' ) => '',
						__( 'Normal', 'fastex_shortcodes' )                             => 'normal',
						__( 'Bold', 'fastex_shortcodes' )                               => 'bold',
						__( 'Bolder', 'fastex_shortcodes' )                             => 'bolder',
						__( 'Lighter', 'fastex_shortcodes' )                            => 'lighter',
					),
					'description' => __( 'Select the description weight.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'description_custom',
						'value'   => 'custom',
					),
				),
				//Description style
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Description style ', 'fastex_shortcodes' ),
					'param_name'  => 'description_style',
					'value'       => array(
						__( 'Choose the description font style', 'fastex_shortcodes' ) => '',
						__( 'Italic', 'fastex_shortcodes' )                            => 'italic',
						__( 'Oblique', 'fastex_shortcodes' )                           => 'oblique',
						__( 'Initial', 'fastex_shortcodes' )                           => 'initial',
						__( 'Inherit', 'fastex_shortcodes' )                           => 'inherit',
					),
					'description' => __( 'Select the description style.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'description_custom',
						'value'   => 'custom',
					),
				),
				// Icon type
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Icon type', 'fastex_shortcodes' ),
					'value'       => array(
						__( 'Choose icon type', 'fastex_shortcodes' ) => '',
						__( 'Single Image', 'fastex_shortcodes' )     => 'image',
						__( 'Font Awesome', 'fastex_shortcodes' )     => 'fontawesome',
						__( 'Openiconic', 'fastex_shortcodes' )       => 'openiconic',
						__( 'Typicons', 'fastex_shortcodes' )         => 'typicons',
						__( 'Entypo', 'fastex_shortcodes' )           => 'entypo',
						__( 'Linecons', 'fastex_shortcodes' )         => 'linecons',
						__( 'Fastex Icon', 'fastex_shortcodes' )      => 'fastex',
					),
					'admin_label' => true,
					'param_name'  => 'icon_type',
					'description' => __( 'Select icon type.', 'fastex_shortcodes' ),
				),

				// Icon type: Image - Image picker
				array(
					'type'        => 'attach_image',
					'heading'     => __( 'Choose image', 'fastex_shortcodes' ),
					'param_name'  => 'icon_image',
					'admin_label' => true,
					'value'       => '',
					'description' => __( 'Upload the custom image icon.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'image',
					),
				),
				//Image size
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Image size', 'fastex_shortcodes' ),
					'param_name'  => 'image_size',
					'admin_label' => true,
					'description' => __( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'fastex_shortcodes' ),

					'dependency' => array(
						'element' => 'icon_type',
						'value'   => 'image',
					),
				),

				// Icon type: Fontawesome - Icon picker
				array(
					'type'        => 'iconpicker',
					'heading'     => __( 'Icon', 'fastex_shortcodes' ),
					'param_name'  => 'icon_fontawesome',
					'value'       => 'fa fa-heart',
					'settings'    => array(
						'emptyIcon'    => false,
						'iconsPerPage' => 50,
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'fontawesome',
					),
					'description' => __( 'FontAwesome library.', 'fastex_shortcodes' ),
				),

				// Icon type: Openiconic - Icon picker
				array(
					'type'        => 'iconpicker',
					'heading'     => __( 'Icon', 'fastex_shortcodes' ),
					'param_name'  => 'icon_openiconic',
					'value'       => 'vc-oi vc-oi-dial',
					'settings'    => array(
						'emptyIcon'    => false,
						'iconsPerPage' => 50,
						'type'         => 'openiconic',
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'openiconic',
					),
					'description' => __( 'Openiconic library.', 'fastex_shortcodes' ),
				),

				// Icon type: Typicons - Icon picker
				array(
					'type'        => 'iconpicker',
					'heading'     => __( 'Icon', 'fastex_shortcodes' ),
					'param_name'  => 'icon_typicons',
					'value'       => 'typcn typcn-adjust-brightness',
					'settings'    => array(
						'emptyIcon'    => false,
						'iconsPerPage' => 50,
						'type'         => 'typicons',
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'typicons',
					),
					'description' => __( 'Typicons library.', 'fastex_shortcodes' ),
				),

				// Icon type: Entypo - Icon picker
				array(
					'type'        => 'iconpicker',
					'heading'     => __( 'Icon', 'fastex_shortcodes' ),
					'param_name'  => 'icon_entypo',
					'value'       => 'entypo-icon entypo-icon-note',
					'settings'    => array(
						'emptyIcon'    => false,
						'iconsPerPage' => 50,
						'type'         => 'entypo',
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'entypo',
					),
					'description' => __( 'Entypo library.', 'fastex_shortcodes' ),
				),

				// Icon type: Lincons - Icon picker
				array(
					'type'        => 'iconpicker',
					'heading'     => __( 'Icon', 'fastex_shortcodes' ),
					'param_name'  => 'icon_linecons',
					'value'       => '',
					'settings'    => array(
						'emptyIcon'    => false,
						'iconsPerPage' => 50,
						'type'         => 'linecons',
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'linecons',
					),
					'description' => __( 'Linecons library.', 'fastex_shortcodes' ),
				),

				// Icon type: Fastex Icon - Icon picker
				array(
					'type'        => 'iconpicker',
					'heading'     => __( 'Icon', 'fastex_shortcodes' ),
					'param_name'  => 'icon_fastex',
					'value'       => 'fastexicon-delivery22',
					'settings'    => array(
						'emptyIcon'    => false,
						'iconsPerPage' => 50,
						'type'         => 'fastex',
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'fastex',
					),
					'description' => __( 'Fastex library.', 'fastex_shortcodes' ),
				),

				//Icon link
				array(
					'type'        => 'vc_link',
					'heading'     => __( 'Icon link', 'fastex_shortcodes' ),
					'param_name'  => 'icon_link',
					'admin_label' => true,
					'description' => __( 'Enter the link for icon.', 'fastex_shortcodes' ),
				),

				//Icon size
				array(
					'type'        => 'number',
					'admin_label' => true,
					'heading'     => __( 'Icon size', 'fastex_shortcodes' ),
					'param_name'  => 'icon_size',
					'value'       => 40,
					'min'         => 16,
					'max'         => 100,
					'suffix'      => 'px',
					'description' => __( 'Select the icon size.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => array( 'fontawesome', 'openiconic', 'typicons', 'entypo', 'linecons', 'fastex' ),
					),
				),

				//Icon color
				array(
					'type'        => 'colorpicker',
					'heading'     => __( 'Icon color', 'fastex_shortcodes' ),
					'param_name'  => 'icon_color',
					'value'       => '#89BA49',
					'description' => __( 'Select the icon color.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => array( 'fontawesome', 'openiconic', 'typicons', 'entypo', 'linecons', 'fastex' ),
					),
				),
				//Display the button?
				array(
					'type'        => 'checkbox',
					'heading'     => __( 'Display the button?', 'fastex_shortcodes' ),
					'param_name'  => 'button_display',
					'value'       => array( __( '', 'fastex_shortcodes' ) => 'yes' ),
					'description' => __( 'Tick it to display the button.', 'fastex_shortcodes' ),
				),
				//Button link
				array(
					'type'        => 'vc_link',
					'heading'     => __( 'Button link', 'fastex_shortcodes' ),
					'param_name'  => 'button_link',
					'value'       => __( '', 'fastex_shortcodes' ),
					'description' => __( 'Write the button link', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'button_display',
						'value'   => 'yes',
					),
				),
				//Button value
				array(
					'type'        => 'textfield',
					'admin_label' => true,
					'heading'     => __( 'Button value', 'fastex_shortcodes' ),
					'param_name'  => 'button_value',
					'value'       => __( '', 'fastex_shortcodes' ),
					'description' => __( 'Write the button value', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'button_display',
						'value'   => 'yes',
					),
				),
				//Background color
				array(
					'type'        => 'colorpicker',
					'heading'     => __( 'Background color', 'fastex_shortcodes' ),
					'param_name'  => 'background_color',
					'value'       => '',
					'description' => __( 'Select the background color.', 'fastex_shortcodes' ),
				),
				//Text Alignment
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Text alignment', 'fastex_shortcodes' ),
					'param_name'  => 'alignment',
					'value'       => array(
						__( 'Choose the text alignment', 'fastex_shortcodes' ) => '',
						__( 'Text at left', 'fastex_shortcodes' )              => 'left',
						__( 'Text at center', 'fastex_shortcodes' )            => 'center',
						__( 'Text at right', 'fastex_shortcodes' )             => 'right',
					),
				),
				// Animation
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Animation', 'fastex_shortcodes' ),
					'param_name'  => 'css_animation',
					'value'       => array(
						__( 'No', 'fastex_shortcodes' )                 => '',
						__( 'Top to bottom', 'fastex_shortcodes' )      => 'top-to-bottom',
						__( 'Bottom to top', 'fastex_shortcodes' )      => 'bottom-to-top',
						__( 'Left to right', 'fastex_shortcodes' )      => 'left-to-right',
						__( 'Right to left', 'fastex_shortcodes' )      => 'right-to-left',
						__( 'Appear from center', 'fastex_shortcodes' ) => 'appear'
					),
					'description' => __( 'Select animation type if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'fastex_shortcodes' )
				),
				// Extra class
				array(
					'type'        => 'textfield',
					'admin_label' => true,
					'heading'     => __( 'Extra class', 'fastex_shortcodes' ),
					'param_name'  => 'el_class',
					'value'       => '',
					'description' => __( 'Add extra class name that will be applied to the icon box, and you can use this class for your customizations.', 'fastex_shortcodes' ),
				),
			)
		)
	);

	// Mapping shortcode Our Team
	vc_map( array(
		'name'        => __( 'Thim Our Team', 'fastex_shortcodes' ),
		'base'        => 'thim-our-team',
		'category'    => __( 'Thim Shortcodes', 'fastex_shortcodes' ),
		'description' => __( 'Display our team.', 'fastex_shortcodes' ),
		'params'      => array(
			//Number of staffs
			array(
				'type'        => 'number',
				'admin_label' => true,
				'heading'     => __( 'Number', 'fastex_shortcodes' ),
				'param_name'  => 'number',
				'min'         => 0,
				'max'         => 20,
				'value'       => '',
				'description' => __( 'The number of staffs to show.', 'fastex_shortcodes' )
			),
			//Number of column to show
			array(
				'type'        => 'number',
				'admin_label' => true,
				'heading'     => __( 'Columns', 'fastex_shortcodes' ),
				'param_name'  => 'column',
				'min'         => 1,
				'max'         => 4,
				'value'       => 4,
				'description' => __( 'The number of columns per row to show.', 'fastex_shortcodes' )
			),
			//Animation
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Animation', 'fastex_shortcodes' ),
				'param_name'  => 'css_animation',
				'admin_label' => true,
				'value'       => array(
					__( 'No', 'fastex_shortcodes' )                 => '',
					__( 'Top to bottom', 'fastex_shortcodes' )      => 'top-to-bottom',
					__( 'Bottom to top', 'fastex_shortcodes' )      => 'bottom-to-top',
					__( 'Left to right', 'fastex_shortcodes' )      => 'left-to-right',
					__( 'Right to left', 'fastex_shortcodes' )      => 'right-to-left',
					__( 'Appear from center', 'fastex_shortcodes' ) => 'appear'
				),
				'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'fastex_shortcodes' )
			),
			// Extra class
			array(
				'type'        => 'textfield',
				'admin_label' => true,
				'heading'     => __( 'Extra class', 'fastex_shortcodes' ),
				'param_name'  => 'el_class',
				'value'       => '',
				'description' => __( 'Add extra class name that will be applied to the icon box, and you can use this class for your customizations.', 'fastex_shortcodes' ),
			),
		)
	) );

	// Mapping shortcode Recent Posts
	vc_map( array(
		'name'        => __( 'Thim Recent Posts', 'fastex_shortcodes' ),
		'base'        => 'thim-recent-posts',
		'class'       => '',
		'category'    => __( 'Thim Shortcodes', 'fastex_shortcodes' ),
		'description' => __( 'Display recent posts.', 'fastex_shortcodes' ),
		'params'      => array(
			// Number of posts
			array(
				'type'        => 'number',
				'value'       => 3,
				'min'         => 1,
				'admin_label' => true,
				'heading'     => __( 'Number', 'fastex_shortcodes' ),
				'param_name'  => 'number',
				'description' => __( 'Enter number of posts to show.', 'fastex_shortcodes' ),
			),

			// Filter by category
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Filter posts', 'fastex_shortcodes' ),
				'param_name'  => 'cat',
				'admin_label' => true,
				'value'       => ts_get_categories(),
				'description' => __( 'Filter by categories. No filter will get all latest posts.', 'fastex_shortcodes' )
			),


			// Title of blog page
			array(
				'type'        => 'textfield',
				'admin_label' => true,
				'value'       => '',
				'heading'     => __( 'Title', 'fastex_shortcodes' ),
				'param_name'  => 'title',
				'description' => __( 'Title of blog page link. Leave empty to hide the link.', 'fastex_shortcodes' ),
			),
			// Animation
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Animation', 'fastex_shortcodes' ),
				'param_name'  => 'css_animation',
				'admin_label' => true,
				'value'       => array(
					__( 'No', 'fastex_shortcodes' )                 => '',
					__( 'Top to bottom', 'fastex_shortcodes' )      => 'top-to-bottom',
					__( 'Bottom to top', 'fastex_shortcodes' )      => 'bottom-to-top',
					__( 'Left to right', 'fastex_shortcodes' )      => 'left-to-right',
					__( 'Right to left', 'fastex_shortcodes' )      => 'right-to-left',
					__( 'Appear from center', 'fastex_shortcodes' ) => 'appear'
				),
				'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'fastex_shortcodes' )
			),

			// Extra class
			array(
				'type'        => 'textfield',
				'admin_label' => true,
				'heading'     => __( 'Extra class', 'fastex_shortcodes' ),
				'param_name'  => 'el_class',
				'value'       => '',
				'description' => __( 'Add extra class name that will be applied to the icon box, and you can use this class for your customizations.', 'fastex_shortcodes' ),
			),
		)
	) );

	// Mapping shortcode Heading
	vc_map( array(
		'name'        => __( 'Thim Heading', 'fastex_shortcodes' ),
		'base'        => 'thim-heading',
		'category'    => __( 'Thim Shortcodes', 'fastex_shortcodes' ),
		'description' => __( 'Display heading.', 'fastex_shortcodes' ),
		'params'      => array(
			//Title
			array(
				'type'        => 'textfield',
				'admin_label' => true,
				'heading'     => __( 'Title', 'fastex_shortcodes' ),
				'param_name'  => 'title',
				'value'       => __( '', 'fastex_shortcodes' ),
				'description' => __( 'Write the title for the heading.', 'fastex_shortcodes' )
			),
			//Use custom or default title?
			array(
				'type'        => 'dropdown',
				'admin_label' => true,
				'heading'     => __( 'Use custom or default title?', 'fastex_shortcodes' ),
				'param_name'  => 'title_custom',
				'value'       => array(
					__( 'Default', 'fastex_shortcodes' ) => '',
					__( 'Custom', 'fastex_shortcodes' )  => 'custom',
				),
				'description' => __( 'If you select default you will use default title which customized in typography.', 'fastex_shortcodes' )
			),
			//Heading
			array(
				'type'        => 'dropdown',
				'admin_label' => true,
				'heading'     => __( 'Heading tag', 'fastex_shortcodes' ),
				'param_name'  => 'heading_tag',
				'value'       => array(
					'h2' => 'h2',
					'h3' => 'h3',
					'h4' => 'h4',
					'h5' => 'h5',
					'h6' => 'h6',
				),
				'description' => __( 'Choose heading element.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'title_custom',
					'value'   => 'custom',
				),
			),
			//Title color
			array(
				'type'        => 'colorpicker',
				'admin_label' => true,
				'heading'     => __( 'Title color ', 'fastex_shortcodes' ),
				'param_name'  => 'title_color',
				'value'       => __( '', 'fastex_shortcodes' ),
				'description' => __( 'Select the title color.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'title_custom',
					'value'   => 'custom',
				),
			),
			//Title size
			array(
				'type'        => 'number',
				'admin_label' => true,
				'heading'     => __( 'Title size ', 'fastex_shortcodes' ),
				'param_name'  => 'title_size',
				'min'         => 0,
				'value'       => '',
				'suffix'      => 'px',
				'description' => __( 'Select the title size.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'title_custom',
					'value'   => 'custom',
				),
			),
			//Title weight
			array(
				'type'        => 'dropdown',
				'admin_label' => true,
				'heading'     => __( 'Title weight ', 'fastex_shortcodes' ),
				'param_name'  => 'title_weight',
				'value'       => array(
					__( 'Choose the title font weight', 'fastex_shortcodes' ) => '',
					__( 'Normal', 'fastex_shortcodes' )                       => 'normal',
					__( 'Bold', 'fastex_shortcodes' )                         => 'bold',
					__( 'Bolder', 'fastex_shortcodes' )                       => 'bolder',
					__( 'Lighter', 'fastex_shortcodes' )                      => 'lighter',
				),
				'description' => __( 'Select the title weight.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'title_custom',
					'value'   => 'custom',
				),
			),
			//Title style
			array(
				'type'        => 'dropdown',
				'admin_label' => true,
				'heading'     => __( 'Title style ', 'fastex_shortcodes' ),
				'param_name'  => 'title_style',
				'value'       => array(
					__( 'Choose the title font style', 'fastex_shortcodes' ) => '',
					__( 'Italic', 'fastex_shortcodes' )                      => 'italic',
					__( 'Oblique', 'fastex_shortcodes' )                     => 'oblique',
					__( 'Initial', 'fastex_shortcodes' )                     => 'initial',
					__( 'Inherit', 'fastex_shortcodes' )                     => 'inherit',
				),
				'description' => __( 'Select the title style.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'title_custom',
					'value'   => 'custom',
				),
			),
			//Display the separator?
			array(
				'type'        => 'checkbox',
				'admin_label' => true,
				'heading'     => __( 'Hide the separator?', 'fastex_shortcodes' ),
				'param_name'  => 'display',
				'value'       => array( __( '', 'fastex_shortcodes' ) => 'yes' ),
				'description' => __( 'Tick it to hide the separator between title and description.', 'fastex_shortcodes' ),
			),
			//Separator color
			array(
				'type'        => 'colorpicker',
				'heading'     => __( 'Separator color', 'fastex_shortcodes' ),
				'param_name'  => 'separator_color',
				'value'       => __( '', 'fastex_shortcodes' ),
				'description' => __( 'Choose the separator color.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element'            => 'display',
					'value_not_equal_to' => 'yes',
				),
			),
			// Description
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Description', 'fastex_shortcodes' ),
				'param_name'  => 'description',
				'value'       => __( '', 'fastex_shortcodes' ),
				'description' => __( 'Provide the description for this icon box.', 'fastex_shortcodes' )
			),
			//Use custom or default description ?
			array(
				'type'        => 'dropdown',
				'admin_label' => true,
				'heading'     => __( 'Use custom or default description?', 'fastex_shortcodes' ),
				'param_name'  => 'description_custom',
				'value'       => array(
					__( 'Default', 'fastex_shortcodes' ) => '',
					__( 'Custom', 'fastex_shortcodes' )  => 'custom',
				),
				'description' => __( 'If you select default you will use default description which customized in typography.', 'fastex_shortcodes' )
			),

			//Description color
			array(
				'type'        => 'colorpicker',
				'admin_label' => true,
				'heading'     => __( 'Description color ', 'fastex_shortcodes' ),
				'param_name'  => 'description_color',
				'value'       => __( '', 'fastex_shortcodes' ),
				'description' => __( 'Select the description color.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'description_custom',
					'value'   => 'custom',
				),
			),
			//Description size
			array(
				'type'        => 'number',
				'admin_label' => true,
				'heading'     => __( 'Description size ', 'fastex_shortcodes' ),
				'param_name'  => 'description_size',
				'min'         => 0,
				'value'       => '',
				'suffix'      => 'px',
				'description' => __( 'Select the description size.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'description_custom',
					'value'   => 'custom',
				),
			),
			//Description weight
			array(
				'type'        => 'dropdown',
				'admin_label' => true,
				'heading'     => __( 'Description weight ', 'fastex_shortcodes' ),
				'param_name'  => 'Description_weight',
				'value'       => array(
					__( 'Choose the description font weight', 'fastex_shortcodes' ) => '',
					__( 'Normal', 'fastex_shortcodes' )                             => 'normal',
					__( 'Bold', 'fastex_shortcodes' )                               => 'bold',
					__( 'Bolder', 'fastex_shortcodes' )                             => 'bolder',
					__( 'Lighter', 'fastex_shortcodes' )                            => 'lighter',
				),
				'description' => __( 'Select the description weight.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'description_custom',
					'value'   => 'custom',
				),
			),
			//Description style
			array(
				'type'        => 'dropdown',
				'admin_label' => true,
				'heading'     => __( 'Description style ', 'fastex_shortcodes' ),
				'param_name'  => 'description_style',
				'value'       => array(
					__( 'Choose the description font style', 'fastex_shortcodes' ) => '',
					__( 'Italic', 'fastex_shortcodes' )                            => 'italic',
					__( 'Oblique', 'fastex_shortcodes' )                           => 'oblique',
					__( 'Initial', 'fastex_shortcodes' )                           => 'initial',
					__( 'Inherit', 'fastex_shortcodes' )                           => 'inherit',
				),
				'description' => __( 'Select the description style.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'description_custom',
					'value'   => 'custom',
				),
			),
			//Alignment
			array(
				'type'        => 'dropdown',
				'admin_label' => true,
				'heading'     => __( 'Text alignment', 'fastex_shortcodes' ),
				'param_name'  => 'alignment',
				'value'       => array(
					'Choose the text alignment'               => '',
					__( 'Text at left', 'fastex_shortcodes' )   => 'left',
					__( 'Text at center', 'fastex_shortcodes' ) => 'center',
					__( 'Text at right', 'fastex_shortcodes' )  => 'right',
				),
			),
			//Animation
			array(
				"type"        => "dropdown",
				"heading"     => __( "Animation", "fastex" ),
				"param_name"  => "css_animation",
				"admin_label" => true,
				"value"       => array(
					__( "No", "fastex" )                 => '',
					__( "Top to bottom", "fastex" )      => "top-to-bottom",
					__( "Bottom to top", "fastex" )      => "bottom-to-top",
					__( "Left to right", "fastex" )      => "left-to-right",
					__( "Right to left", "fastex" )      => "right-to-left",
					__( "Appear from center", "fastex" ) => "appear"
				),
				"description" => __( "Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", "fastex" )
			),
			// Extra class
			array(
				'type'        => 'textfield',
				'admin_label' => true,
				'heading'     => __( 'Extra class', 'fastex_shortcodes' ),
				'param_name'  => 'el_class',
				'value'       => '',
				'description' => __( 'Add extra class name that will be applied to the icon box, and you can use this class for your customizations.', 'fastex_shortcodes' ),
			),
		)
	) );

	// Mapping shortcode Testimonials
	vc_map( array(
		'name'        => __( 'Thim Testimonials', 'fastex_shortcodes' ),
		'base'        => 'thim-testimonials',
		'class'       => '',
		'category'    => __( 'Thim Shortcodes', 'fastex_shortcodes' ),
		'description' => __( 'Display testimonials.', 'fastex_shortcodes' ),
		'params'      => array(
			array(
				//Number of testimonials
				'type'        => 'number',
				'admin_label' => true,
				'heading'     => __( 'Number of posts', 'fastex_shortcodes' ),
				'param_name'  => 'number',
				'value'       => '',
				'min'         => 0,
				'max'         => 15,
				'description' => __( 'The number of testimonials you want to display.', 'fastex_shortcodes' )
			),
			//Animation
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Animation', 'fastex_shortcodes' ),
				'param_name'  => 'css_animation',
				'admin_label' => true,
				'value'       => array(
					__( 'No', 'fastex_shortcodes' )                 => '',
					__( 'Top to bottom', 'fastex_shortcodes' )      => 'top-to-bottom',
					__( 'Bottom to top', 'fastex_shortcodes' )      => 'bottom-to-top',
					__( 'Left to right', 'fastex_shortcodes' )      => 'left-to-right',
					__( 'Right to left', 'fastex_shortcodes' )      => 'right-to-left',
					__( 'Appear from center', 'fastex_shortcodes' ) => 'appear'
				),
				'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'fastex_shortcodes' )
			),
			// Extra class
			array(
				'type'        => 'textfield',
				'admin_label' => true,
				'heading'     => __( 'Extra class', 'fastex_shortcodes' ),
				'param_name'  => 'el_class',
				'value'       => '',
				'description' => __( 'Add extra class name that will be applied to the icon box, and you can use this class for your customizations.', 'fastex_shortcodes' ),
			),
		)
	) );


	// Mapping shortcode Child Pages
	vc_map( array(
		'name'        => __( 'Thim Child Pages', 'fastex_shortcodes' ),
		'base'        => 'thim-child-pages',
		'class'       => '',
		'category'    => __( 'Thim Shortcodes', 'fastex_shortcodes' ),
		'description' => __( 'Display all child pages of current page.', 'fastex_shortcodes' ),
		'params'      => array(

			// Grid columns
			array(
				'type'        => 'number',
				'value'       => 2,
				'admin_label' => true,
				'min'         => 1,
				'max'         => 4,
				'heading'     => __( 'Number', 'fastex_shortcodes' ),
				'param_name'  => 'columns',
				'description' => __( 'Enter number of grid columns.', 'fastex_shortcodes' ),
			),
			// Animation
			array(
				'type'        => 'dropdown',
				'admin_label' => true,
				'heading'     => __( 'Animation', 'fastex_shortcodes' ),
				'param_name'  => 'animation',
				'value'       => array(
					__( 'No', 'fastex_shortcodes' )                 => '',
					__( 'Top to bottom', 'fastex_shortcodes' )      => 'top-to-bottom',
					__( 'Bottom to top', 'fastex_shortcodes' )      => 'bottom-to-top',
					__( 'Left to right', 'fastex_shortcodes' )      => 'left-to-right',
					__( 'Right to left', 'fastex_shortcodes' )      => 'right-to-left',
					__( 'Appear from center', 'fastex_shortcodes' ) => 'appear'
				),
				'description' => __( 'Select animation type if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'fastex_shortcodes' )
			),
			// Extra class
			array(
				'type'        => 'textfield',
				'admin_label' => true,
				'heading'     => __( 'Extra class', 'fastex_shortcodes' ),
				'param_name'  => 'el_class',
				'value'       => '',
				'description' => __( 'Add extra class name that will be applied to the icon box, and you can use this class for your customizations.', 'fastex_shortcodes' ),
			),
		)
	) );

	// Mapping shortcode Counter Box
	vc_map( array(
		'name'        => __( 'Thim Counter Box', 'fastex_shortcodes' ),
		'base'        => 'thim-counter-box',
		'class'       => '',
		'category'    => __( 'Thim Shortcodes', 'fastex_shortcodes' ),
		'description' => __( 'Display counter box.', 'fastex_shortcodes' ),
		'params'      => array(

			//Circle box color
			array(
				'type'        => 'colorpicker',
				'heading'     => __( 'Circle color', 'fastex_shortcodes' ),
				'param_name'  => 'b_color',
				'value'       => __( '', 'fastex_shortcodes' ),
				'description' => __( 'Select the circle box background color', 'fastex_shortcodes' )
			),
			// Count to number
			array(
				'type'        => 'number',
				'admin_label' => true,
				'value'       => 10,
				'min'         => 0,
				'heading'     => __( 'Number', 'fastex_shortcodes' ),
				'param_name'  => 'number',
				'description' => __( 'Enter number in box to count.', 'fastex_shortcodes' ),
			),
			//Number color
			array(
				'type'        => 'colorpicker',
				'heading'     => __( 'Number color', 'fastex_shortcodes' ),
				'param_name'  => 'number_color',
				'value'       => __( '', 'fastex_shortcodes' ),
				'description' => __( 'Select the number color', 'fastex_shortcodes' )
			),
			// Text
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Text', 'fastex_shortcodes' ),
				'admin_label' => true,
				'param_name'  => 'text',
				'value'       => '',
				'description' => __( 'Short text in counter box.', 'fastex_shortcodes' ),
			),
			//Text color
			array(
				'type'        => 'colorpicker',
				'heading'     => __( 'Text color', 'fastex_shortcodes' ),
				'param_name'  => 'text_color',
				'value'       => __( '', 'fastex_shortcodes' ),
				'description' => __( 'Select the text color', 'fastex_shortcodes' )
			),
			// Extra class
			array(
				'type'        => 'textfield',
				'admin_label' => true,
				'heading'     => __( 'Extra class', 'fastex_shortcodes' ),
				'param_name'  => 'el_class',
				'value'       => '',
				'description' => __( 'Add extra class name that will be applied to the icon box, and you can use this class for your customizations.', 'fastex_shortcodes' ),
			),
		)
	) );

	// Mapping shortcode Google Map
	vc_map(
		array(
			'name'                    => __( 'Thim Google Map', 'fastex_shortcodes' ),
			'base'                    => 'thim-google-map',
			'category'                => __( 'Thim Shortcodes', 'fastex_shortcodes' ),
			'description'             => __( 'Display Google map.', 'fastex_shortcodes' ),
			'controls'                => 'full',
			'show_settings_on_create' => true,
			'params'                  => array(
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => esc_html__( 'Map Options', 'fastex_shortcodes' ),
					'param_name'  => 'map_option',
					'value'       => array(
						__( 'Google API Key', 'fastex_shortcodes' )    => 'google_api_key',
						__( 'Google Map Iframe', 'fastex_shortcodes' ) => 'google_map_iframe',
					),
				),
				array(
					'type'        => 'textarea_raw_html',
					'heading'     => esc_html__( 'Iframe Map', 'fastex_shortcodes' ),
					'param_name'  => 'iframe_map',
					'value'       => '',
					'dependency'  => array(
						'element' => 'map_option',
						'value'   => 'google_map_iframe',
					),
				),

				// Map center
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Map center', 'fastex_shortcodes' ),
					'param_name'  => 'map_center',
					'admin_label' => true,
					'value'       => '',
					'description' => __( 'The name of a place, town, city, or even a country. Can be an exact address too.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'map_option',
						'value'   => 'google_api_key',
					),
				),
				// Map height
				array(
					'type'        => 'number',
					'admin_label' => true,
					'heading'     => __( 'Height', 'fastex_shortcodes' ),
					'param_name'  => 'height',
					'min'         => 0,
					'value'       => 480,
					'suffix'      => 'px',
					'description' => __( 'Height of the map.', 'fastex_shortcodes' ),
				),

				// Zoom options
				array(
					'type'        => 'number',
					'admin_label' => true,
					'heading'     => __( 'Zoom level', 'fastex_shortcodes' ),
					'param_name'  => 'zoom',
					'min'         => 0,
					'max'         => 21,
					'value'       => 12,
					'description' => __( 'A value from 0 (the world) to 21 (street level).', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'map_option',
						'value'   => 'google_api_key',
					),
				),

				// Show marker
				array(
					'type'        => 'checkbox',
					'heading'     => __( 'Marker', 'fastex_shortcodes' ),
					'param_name'  => 'marker_at_center',
					'value'       => array( '' => 'true' ),
					'description' => __( 'Show marker at map center.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'map_option',
						'value'   => 'google_api_key',
					),
				),

				// Get marker
				array(
					'type'        => 'attach_image',
					'heading'     => __( 'Choose marker icon', 'fastex_shortcodes' ),
					'param_name'  => 'marker_icon',
					'admin_label' => true,
					'value'       => '',
					'description' => __( 'Replaces the default map marker with your own image.', 'fastex_shortcodes' ),
					'dependency'  => array( 'element' => 'marker_at_center', 'value' => array( 'true' ) ),
					'dependency'  => array(
						'element' => 'map_option',
						'value'   => 'google_api_key',
					),
				),

				// Other options
				array(
					'type'        => 'checkbox',
					'heading'     => __( 'Scroll to zoom', 'fastex_shortcodes' ),
					'param_name'  => 'scroll_zoom',
					'value'       => array( '' => 'true' ),
					'description' => __( 'Allow scrolling over the map to zoom in or out.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'map_option',
						'value'   => 'google_api_key',
					),
				),

				// Other options
				array(
					'type'        => 'checkbox',
					'heading'     => __( 'Draggable', 'fastex_shortcodes' ),
					'param_name'  => 'draggable',
					'value'       => array( '' => 'true' ),
					'description' => __( 'Allow dragging the map to move it around.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'map_option',
						'value'   => 'google_api_key',
					),
				),
				// Animation
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Animation', 'fastex_shortcodes' ),
					'param_name'  => 'animation',
					'value'       => array(
						__( 'No', 'fastex_shortcodes' )                 => '',
						__( 'Top to bottom', 'fastex_shortcodes' )      => 'top-to-bottom',
						__( 'Bottom to top', 'fastex_shortcodes' )      => 'bottom-to-top',
						__( 'Left to right', 'fastex_shortcodes' )      => 'left-to-right',
						__( 'Right to left', 'fastex_shortcodes' )      => 'right-to-left',
						__( 'Appear from center', 'fastex_shortcodes' ) => 'appear'
					),
					'description' => __( 'Select animation type if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'fastex_shortcodes' )
				),
				// Extra class
				array(
					'type'        => 'textfield',
					'admin_label' => true,
					'heading'     => __( 'Extra class', 'fastex_shortcodes' ),
					'param_name'  => 'el_class',
					'value'       => '',
					'description' => __( 'Add extra class name that will be applied to the icon box, and you can use this class for your customizations.', 'fastex_shortcodes' ),
				),

			)
		)
	);

	// Mapping shortcode Gallery
	vc_map(
		array(
			'name'                    => __( 'Thim Gallery', 'fastex_shortcodes' ),
			'base'                    => 'thim-gallery',
			'category'                => __( 'Thim Shortcodes', 'fastex_shortcodes' ),
			'description'             => __( 'Display all format gallery.', 'fastex_shortcodes' ),
			'controls'                => 'full',
			'show_settings_on_create' => true,
			'params'                  => array(

				// Gallery grid columns
				array(
					'type'        => 'number',
					'admin_label' => true,
					'heading'     => __( 'Number of columns', 'fastex_shortcodes' ),
					'param_name'  => 'cell',
					'min'         => 1,
					'max'         => 4,
					'value'       => 3,
					'description' => __( 'Gallery shortcode supports maximum 4 columns.', 'fastex_shortcodes' ),
				),

				// Image size
				array(
					'type'        => 'textfield',
					'admin_label' => true,
					'heading'     => __( 'Image size', 'fastex_shortcodes' ),
					'param_name'  => 'image_size',
					'description' => __( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'fastex_shortcodes' ),
				),

				// Layout
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Layout', 'fastex_shortcodes' ),
					'param_name'  => 'layout',
					'value'       => array(
						__( 'Filter', 'fastex_shortcodes' )    => 'filter',
						__( 'No filter', 'fastex_shortcodes' ) => 'no-filter',
					),
					'description' => __( 'Choose layout.', 'fastex_shortcodes' )
				),

				// Limit images
				array(
					'type'        => 'number',
					'min'         => 1,
					'value'       => 9,
					'admin_label' => true,
					'heading'     => __( 'Limit images', 'fastex_shortcodes' ),
					'param_name'  => 'limit',
					'description' => __( 'Number of images in gallery to display.', 'fastex_shortcodes' ),
					'dependency'  => array(
						'element' => 'layout',
						'value'   => 'no-filter',
					),
				),

				// Using grid in small device
				array(
					'type'        => 'checkbox',
					'admin_label' => true,
					'heading'     => __( 'Using grid in super small device?', 'fastex_shortcodes' ),
					'param_name'  => 'xs-grid',
					'value'       => '',
					'dependency'  => array(
						'element' => 'layout',
						'value'   => 'no-filter',
					),
				),
				// Animation
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Animation', 'fastex_shortcodes' ),
					'param_name'  => 'animation',
					'value'       => array(
						__( 'No', 'fastex_shortcodes' )                 => '',
						__( 'Top to bottom', 'fastex_shortcodes' )      => 'top-to-bottom',
						__( 'Bottom to top', 'fastex_shortcodes' )      => 'bottom-to-top',
						__( 'Left to right', 'fastex_shortcodes' )      => 'left-to-right',
						__( 'Right to left', 'fastex_shortcodes' )      => 'right-to-left',
						__( 'Appear from center', 'fastex_shortcodes' ) => 'appear'
					),
					'description' => __( 'Select animation type if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'fastex_shortcodes' )
				),
				// Extra class
				array(
					'type'        => 'textfield',
					'admin_label' => true,
					'heading'     => __( 'Extra class', 'fastex_shortcodes' ),
					'param_name'  => 'el_class',
					'value'       => '',
					'description' => __( 'Add extra class name that will be applied to the icon box, and you can use this class for your customizations.', 'fastex_shortcodes' ),
				),
			)
		)
	);

	// Mapping shortcode Images
	vc_map(
		array(
			'name'                    => __( 'Thim Images', 'fastex_shortcodes' ),
			'base'                    => 'thim-images',
			'category'                => __( 'Thim Shortcodes', 'fastex_shortcodes' ),
			'description'             => __( 'Display images gallery.', 'fastex_shortcodes' ),
			'controls'                => 'full',
			'show_settings_on_create' => true,
			'params'                  => array(

				// Get images
				array(
					'type'        => 'attach_images',
					'heading'     => __( 'Choose images', 'fastex_shortcodes' ),
					'param_name'  => 'images',
					'admin_label' => true,
					'value'       => '',
					'description' => __( 'Choose images from media library.', 'fastex_shortcodes' ),
				),

				// Images grid columns
				array(
					'type'        => 'number',
					'admin_label' => true,
					'heading'     => __( 'Number of columns', 'fastex_shortcodes' ),
					'param_name'  => 'cell',
					'min'         => 1,
					'max'         => 12,
					'value'       => 5,
				),

				// Image size
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Image size', 'fastex_shortcodes' ),
					'param_name'  => 'size',
					'admin_label' => true,
					'description' => __( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'fastex_shortcodes' ),
				),

				// Animation
				array(
					'type'        => 'dropdown',
					'admin_label' => true,
					'heading'     => __( 'Animation', 'fastex_shortcodes' ),
					'param_name'  => 'animation',
					'value'       => array(
						__( 'No', 'fastex_shortcodes' )                 => '',
						__( 'Top to bottom', 'fastex_shortcodes' )      => 'top-to-bottom',
						__( 'Bottom to top', 'fastex_shortcodes' )      => 'bottom-to-top',
						__( 'Left to right', 'fastex_shortcodes' )      => 'left-to-right',
						__( 'Right to left', 'fastex_shortcodes' )      => 'right-to-left',
						__( 'Appear from center', 'fastex_shortcodes' ) => 'appear'
					),
					'description' => __( 'Select animation type if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'fastex_shortcodes' )
				),
				// Extra class
				array(
					'type'        => 'textfield',
					'admin_label' => true,
					'heading'     => __( 'Extra class', 'fastex_shortcodes' ),
					'param_name'  => 'el_class',
					'value'       => '',
					'description' => __( 'Add extra class name that will be applied to the icon box, and you can use this class for your customizations.', 'fastex_shortcodes' ),
				),
			)
		)
	);

}

add_action( 'vc_before_init', 'ts_map_vc_shortcodes' );
