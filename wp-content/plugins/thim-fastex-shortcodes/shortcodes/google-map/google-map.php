<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Shortcode Google Map
 *
 * @param $atts
 *
 * @return string
 */
function ts_shortcode_google_map( $atts ) {


	$google_map = shortcode_atts( array(
		'title'            => __( 'Our Address', 'fastex_shortcodes' ),
		'map_option'       => 'google_api_key',
		'iframe_map'       => '',
		'map_center'       => '',
		'height'           => '',
		'zoom'             => '',
		'scroll_zoom'      => '',
		'draggable'        => '',
		'marker_at_center' => '',
		'marker_icon'      => '',
		'animation'        => '',
		'el_class'         => '',
	), $atts );

	$animation = thim_getCSSAnimation( $google_map['animation'] );
	$height    = $google_map['height'] . 'px';
	if ( $google_map['map_option'] == 'google_api_key' ) {
		wp_enqueue_script( 'ts-google-map', TS_URL . 'js/google-map.js', array( 'jquery' ), '', true );
		// Get settings
		$id         = ' id="ob-map-canvas-' . md5( $google_map['map_center'] ) . '"';
		$data       = 'data-address="' . $google_map['map_center'] . '" ';
		$data       .= 'data-zoom="' . $google_map['zoom'] . '" ';
		$data       .= 'data-scroll-zoom="' . $google_map['scroll_zoom'] . '" ';
		$data       .= 'data-draggable="' . $google_map['draggable'] . '" ';
		$data       .= 'data-marker-at-center="' . $google_map['marker_at_center'] . '" ';
		$data       .= isset( $google_map['map_api'] ) ? 'data-google-map-api="' . $google_map['map_api'] . '" ' : '';
		$icon_src   = wp_get_attachment_image_src( $google_map['marker_icon'] );
		$icon       = isset( $icon_src[0] ) ? $icon_src[0] : '';
		$data       .= 'data-marker-icon="' . $icon . '" ';
		$class      = 'ob-google-map-canvas';
		$inner_html = '';
	} else {
		$data       = $id = '';
		$class      = 'ob-google-map-iframe';
		$inner_html = rawurldecode( base64_decode( $google_map['iframe_map'] ) );
	}


	$html = '<div class="' . $class . ' ' . $animation . ' ' . esc_attr( $google_map['el_class'] ) . '"' . $id . ' style="height: ' . $height . ';" ' . $data . ' >' . $inner_html . '</div>';

	return $html;
}

add_shortcode( 'thim-google-map', 'ts_shortcode_google_map' );

