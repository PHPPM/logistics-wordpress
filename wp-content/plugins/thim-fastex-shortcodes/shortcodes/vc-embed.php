<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Embed custom Fastex Icon for Visual Composer system
 */
function ts_embed_icon() {

	// Embed to shortcode VC Icon
	global $vc_add_css_animation;
	$settings = array(
		'params' => array(
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Icon library', 'fastex_shortcodes' ),
				'value'       => array(
					__( 'Font Awesome', 'fastex_shortcodes' )  => 'fontawesome',
					__( 'Open Iconic', 'fastex_shortcodes' )   => 'openiconic',
					__( 'Typicons', 'fastex_shortcodes' )      => 'typicons',
					__( 'Entypo', 'fastex_shortcodes' )        => 'entypo',
					__( 'Linecons', 'fastex_shortcodes' )      => 'linecons',
					__( 'Fastex Icons', 'fastex_shortcodes' ) => 'fastex',
				),
				'admin_label' => true,
				'param_name'  => 'type',
				'description' => __( 'Select icon library.', 'fastex_shortcodes' ),
			),

			// Fastex Icon
			array(
				'type'        => 'iconpicker',
				'heading'     => __( 'Icon', 'fastex_shortcodes' ),
				'param_name'  => 'icon_fastex',
				'value'       => 'fastexicon-delivery22',
				'settings'    => array(
					'emptyIcon'    => false,
					'iconsPerPage' => 50,
					'type'         => 'fastex',
				),
				'dependency'  => array(
					'element' => 'type',
					'value'   => 'fastex',
				),
				'description' => __( 'Select icon from library.', 'fastex_shortcodes' ),
			),

			array(
				'type'        => 'iconpicker',
				'heading'     => __( 'Icon', 'fastex_shortcodes' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => 'fa fa-adjust', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => false,
					// default true, display an "EMPTY" icon?
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency'  => array(
					'element' => 'type',
					'value'   => 'fontawesome',
				),
				'description' => __( 'Select icon from library.', 'fastex_shortcodes' ),
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => __( 'Icon', 'fastex_shortcodes' ),
				'param_name'  => 'icon_openiconic',
				'value'       => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => false, // default true, display an "EMPTY" icon?
					'type'         => 'openiconic',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency'  => array(
					'element' => 'type',
					'value'   => 'openiconic',
				),
				'description' => __( 'Select icon from library.', 'fastex_shortcodes' ),
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => __( 'Icon', 'fastex_shortcodes' ),
				'param_name'  => 'icon_typicons',
				'value'       => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => false, // default true, display an "EMPTY" icon?
					'type'         => 'typicons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency'  => array(
					'element' => 'type',
					'value'   => 'typicons',
				),
				'description' => __( 'Select icon from library.', 'fastex_shortcodes' ),
			),
			array(
				'type'       => 'iconpicker',
				'heading'    => __( 'Icon', 'fastex_shortcodes' ),
				'param_name' => 'icon_entypo',
				'value'      => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
				'settings'   => array(
					'emptyIcon'    => false, // default true, display an "EMPTY" icon?
					'type'         => 'entypo',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value'   => 'entypo',
				),
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => __( 'Icon', 'fastex_shortcodes' ),
				'param_name'  => 'icon_linecons',
				'value'       => 'vc_li vc_li-heart', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => false, // default true, display an "EMPTY" icon?
					'type'         => 'linecons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency'  => array(
					'element' => 'type',
					'value'   => 'linecons',
				),
				'description' => __( 'Select icon from library.', 'fastex_shortcodes' ),
			),
			array(
				'type'               => 'dropdown',
				'heading'            => __( 'Icon color', 'fastex_shortcodes' ),
				'param_name'         => 'color',
				'value'              => array_merge( getVcShared( 'colors' ), array( __( 'Custom color', 'fastex_shortcodes' ) => 'custom' ) ),
				'description'        => __( 'Select icon color.', 'fastex_shortcodes' ),
				'param_holder_class' => 'vc_colored-dropdown',
			),
			array(
				'type'        => 'colorpicker',
				'heading'     => __( 'Custom color', 'fastex_shortcodes' ),
				'param_name'  => 'custom_color',
				'description' => __( 'Select custom icon color.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'color',
					'value'   => 'custom',
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Background shape', 'fastex_shortcodes' ),
				'param_name'  => 'background_style',
				'value'       => array(
					__( 'None', 'fastex_shortcodes' )            => '',
					__( 'Circle', 'fastex_shortcodes' )          => 'rounded',
					__( 'Square', 'fastex_shortcodes' )          => 'boxed',
					__( 'Rounded', 'fastex_shortcodes' )         => 'rounded-less',
					__( 'Outline Circle', 'fastex_shortcodes' )  => 'rounded-outline',
					__( 'Outline Square', 'fastex_shortcodes' )  => 'boxed-outline',
					__( 'Outline Rounded', 'fastex_shortcodes' ) => 'rounded-less-outline',
				),
				'description' => __( 'Select background shape and style for icon.', 'fastex_shortcodes' )
			),
			array(
				'type'               => 'dropdown',
				'heading'            => __( 'Background color', 'fastex_shortcodes' ),
				'param_name'         => 'background_color',
				'value'              => array_merge( getVcShared( 'colors' ), array( __( 'Custom color', 'fastex_shortcodes' ) => 'custom' ) ),
				'std'                => 'grey',
				'description'        => __( 'Select background color for icon.', 'fastex_shortcodes' ),
				'param_holder_class' => 'vc_colored-dropdown',
				'dependency'         => array(
					'element'   => 'background_style',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'colorpicker',
				'heading'     => __( 'Custom background color', 'fastex_shortcodes' ),
				'param_name'  => 'custom_background_color',
				'description' => __( 'Select custom icon background color.', 'fastex_shortcodes' ),
				'dependency'  => array(
					'element' => 'background_color',
					'value'   => 'custom',
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Size', 'fastex_shortcodes' ),
				'param_name'  => 'size',
				'value'       => array_merge( getVcShared( 'sizes' ), array( 'Extra Large' => 'xl' ) ),
				'std'         => 'md',
				'description' => __( 'Icon size.', 'fastex_shortcodes' )
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Icon alignment', 'fastex_shortcodes' ),
				'param_name'  => 'align',
				'value'       => array(
					__( 'Left', 'fastex_shortcodes' )   => 'left',
					__( 'Right', 'fastex_shortcodes' )  => 'right',
					__( 'Center', 'fastex_shortcodes' ) => 'center',
				),
				'description' => __( 'Select icon alignment.', 'fastex_shortcodes' ),
			),
			array(
				'type'        => 'vc_link',
				'heading'     => __( 'URL (Link)', 'fastex_shortcodes' ),
				'param_name'  => 'link',
				'description' => __( 'Add link to icon.', 'fastex_shortcodes' )
			),
			$vc_add_css_animation,
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Extra class name', 'fastex_shortcodes' ),
				'param_name'  => 'el_class',
				'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'fastex_shortcodes' )
			),
			array(
				'type' => 'css_editor',
				'heading' => __( 'CSS box', 'fastex_shortcodes' ),
				'param_name' => 'css',
				'group' => __( 'Design Options', 'fastex_shortcodes' )
			),

		),
	);
	vc_map_update( 'vc_icon', $settings );

	// Embed to shortcode VC section (of tab)
	$new_icon_params = vc_map_integrate_shortcode(
		'vc_icon',
		'i_',
		'',
		array(
			'include_only_regex' => '/^(type|icon_\w*)/',
		), array(
			'element' => 'add_icon',
			'value'   => 'true',
		)
	);
	$settings        = array(
		'params' => array_merge( array(
			array(
				'type'        => 'textfield',
				'param_name'  => 'title',
				'heading'     => __( 'Title', 'fastex_shortcodes' ),
				'description' => __( 'Enter section title (Note: you can leave it empty).', 'fastex_shortcodes' ),
			),
			array(
				'type'        => 'el_id',
				'param_name'  => 'tab_id',
				'settings'    => array(
					'auto_generate' => true,
				),
				'heading'     => __( 'Section ID', 'fastex_shortcodes' ),
				'description' => __( 'Enter section ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'fastex_shortcodes' ),
			),
			array(
				'type'        => 'checkbox',
				'param_name'  => 'add_icon',
				'heading'     => __( 'Add icon?', 'fastex_shortcodes' ),
				'description' => __( 'Add icon next to section title.', 'fastex_shortcodes' ),
			),
			array(
				'type'        => 'dropdown',
				'param_name'  => 'i_position',
				'value'       => array(
					__( 'Before title', 'fastex_shortcodes' ) => 'left',
					__( 'After title', 'fastex_shortcodes' )  => 'right',
				),
				'dependency'  => array(
					'element' => 'add_icon',
					'value'   => 'true',
				),
				'heading'     => __( 'Icon position', 'fastex_shortcodes' ),
				'description' => __( 'Select icon position.', 'fastex_shortcodes' ),
			),
		),
			$new_icon_params,
			array(
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Extra class name', 'fastex_shortcodes' ),
					'param_name'  => 'el_class',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'fastex_shortcodes' )
				)
			)
		),
	);
	vc_map_update( 'vc_tta_section', $settings );

}

add_action( 'init', 'ts_embed_icon' );