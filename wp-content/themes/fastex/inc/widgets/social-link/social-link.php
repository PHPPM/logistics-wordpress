<?php

/**
 * Created by PhpStorm.
 * User: Quoc
 * Date: 7/28/15
 * Time: 3:28 PM
 */
class Thim_Social_Link_Widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'social_link_widget',
			__( 'Thim: Social Link', 'fastex' ),
			array( 'description' => __( 'Display a list of social links', 'fastex' ), )
		);
	}

	// Creating widget frontend
	public function widget( $args, $instance ) {
		$list_social = '';

		if ( isset( $instance['facebook'] ) && $instance['facebook'] != '' ) {
			$list_social .= '<li><a href="' . esc_url( $instance['facebook'] ) . '" title="' . esc_attr__( 'Facebook', 'fastex' ) . '"><i class="fa fa-facebook fa-lg"></i></a></li>';
		}

		if ( isset( $instance['google_plus'] ) && $instance['google_plus'] != '' ) {
			$list_social .= '<li><a href="' . esc_url( $instance['google_plus'] ) . '" title="' . esc_attr__( 'Google Plus', 'fastex' ) . '"><i class="fa fa-google-plus fa-lg"></i></a></li>';
		}

		if ( isset( $instance['twitter'] ) && $instance['twitter'] != '' ) {
			$list_social .= '<li><a href="' . esc_url( $instance['twitter'] ) . '" title="' . esc_attr__( 'Twitter', 'fastex' ) . '"><i class="fa fa-twitter fa-lg"></i></a></li>';
		}

		if ( isset( $instance['linkedin'] ) && $instance['linkedin'] != '' ) {
			$list_social .= '<li><a href="' . esc_url( $instance['linkedin'] ) . '" title="' . esc_attr__( 'Linkedin', 'fastex' ) . '"><i class="fa fa-linkedin fa-lg"></i></a></li>';
		}

		if ( isset( $instance['skype'] ) && $instance['skype'] != '' ) {
			$list_social .= '<li><a href="' . esc_url( $instance['skype'] ) . '" title="' . esc_attr__( 'Skype', 'fastex' ) . '"><i class="fa fa-skype fa-lg"></i></a></li>';
		}

		if ( isset( $instance['pinterest'] ) && $instance['pinterest'] != '' ) {
			$list_social .= '<li><a href="' . esc_url( $instance['pinterest'] ) . '" title="' . esc_attr__( 'Pinterest', 'fastex' ) . '"><i class="fa fa-pinterest fa-lg"></i></a></li>';
		}

		if ( isset( $instance['instagram'] ) && $instance['instagram'] != '' ) {
			$list_social .= '<li><a href="' . esc_url( $instance['instagram'] ) . '" title="' . esc_attr__( 'Instagram', 'fastex' ) . '"><i class="fa fa-instagram fa-lg"></i></a></li>';
		}

		if ( isset( $instance['youtube'] ) && $instance['youtube'] != '' ) {
			$list_social .= '<li><a href="' . esc_url( $instance['youtube'] ) . '" title="' . esc_attr__( 'Youtube', 'fastex' ) . '"><i class="fa fa-youtube fa-lg"></i></a></li>';
		}

		if ( isset( $instance['tumblr'] ) && $instance['tumblr'] != '' ) {
			$list_social .= '<li><a href="' . esc_url( $instance['tumblr'] ) . '" title="' . esc_attr__( 'Tumblr', 'fastex' ) . '"><i class="fa fa-tumblr fa-lg"></i></a></li>';
		}

		if ( $list_social != '' ) { ?>
			<div class="social-link">
				<ul>
					<?php echo $list_social; ?>
				</ul>
			</div>
			<?php
		}
	}

	// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance['facebook'] ) ) {
			$facebook = $instance['facebook'];
		} else {
			$facebook = '#';
		}

		if ( isset( $instance['google_plus'] ) ) {
			$google_plus = $instance['google_plus'];
		} else {
			$google_plus = '#';
		}

		if ( isset( $instance['twitter'] ) ) {
			$twitter = $instance['twitter'];
		} else {
			$twitter = '#';
		}

		if ( isset( $instance['pinterest'] ) ) {
			$pinterest = $instance['pinterest'];
		} else {
			$pinterest = '#';
		}

		if ( isset( $instance['skype'] ) ) {
			$skype = $instance['skype'];
		} else {
			$skype = '#';
		}

		if ( isset( $instance['linkedin'] ) ) {
			$linkedin = $instance['linkedin'];
		} else {
			$linkedin = '#';
		}

		if ( isset( $instance['instagram'] ) ) {
			$instagram = $instance['instagram'];
		} else {
			$instagram = '#';
		}

		if ( isset( $instance['youtube'] ) ) {
			$youtube = $instance['youtube'];
		} else {
			$youtube = '#';
		}

		if ( isset( $instance['tumblr'] ) ) {
			$tumblr = $instance['tumblr'];
		} else {
			$tumblr = '#';
		}

		// Widget admin form
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>"><?php esc_html_e( 'Facebook:', 'fastex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'facebook' ) ); ?>" type="text" value="<?php echo esc_attr( $facebook ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'google_plus' ) ); ?>"><?php esc_html_e( 'Google Plus:', 'fastex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'google_plus' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'google_plus' ) ); ?>" type="text" value="<?php echo esc_attr( $google_plus ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>"><?php esc_html_e( 'Twitter:', 'fastex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter' ) ); ?>" type="text" value="<?php echo esc_attr( $twitter ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ); ?>"><?php esc_html_e( 'Linkedin:', 'fastex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'linkedin' ) ); ?>" type="text" value="<?php echo esc_attr( $linkedin ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'pinterest' ) ); ?>"><?php esc_html_e( 'Pinterest:', 'fastex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'pinterest' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'pinterest' ) ); ?>" type="text" value="<?php echo esc_attr( $pinterest ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'skype' ) ); ?>"><?php esc_html_e( 'Skype:', 'fastex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'skype' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'skype' ) ); ?>" type="text" value="<?php echo esc_attr( $skype ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>"><?php esc_html_e( 'Instagram:', 'fastex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'instagram' ) ); ?>" type="text" value="<?php echo esc_attr( $instagram ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>"><?php esc_html_e( 'Youtube:', 'fastex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'youtube' ) ); ?>" type="text" value="<?php echo esc_attr( $youtube ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'tumblr' ) ); ?>"><?php esc_html_e( 'Tumblr:', 'fastex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tumblr' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'tumblr' ) ); ?>" type="text" value="<?php echo esc_attr( $tumblr ); ?>" />
		</p>

		<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance                = array();
		$instance['facebook']    = ( ! empty( $new_instance['facebook'] ) ) ? strip_tags( $new_instance['facebook'] ) : '';
		$instance['google_plus'] = ( ! empty( $new_instance['google_plus'] ) ) ? strip_tags( $new_instance['google_plus'] ) : '';
		$instance['twitter']     = ( ! empty( $new_instance['twitter'] ) ) ? strip_tags( $new_instance['twitter'] ) : '';
		$instance['skype']       = ( ! empty( $new_instance['skype'] ) ) ? strip_tags( $new_instance['skype'] ) : '';
		$instance['pinterest']   = ( ! empty( $new_instance['pinterest'] ) ) ? strip_tags( $new_instance['pinterest'] ) : '';
		$instance['linkedin']    = ( ! empty( $new_instance['linkedin'] ) ) ? strip_tags( $new_instance['linkedin'] ) : '';
		$instance['instagram']   = ( ! empty( $new_instance['instagram'] ) ) ? strip_tags( $new_instance['instagram'] ) : '';
		$instance['youtube']     = ( ! empty( $new_instance['youtube'] ) ) ? strip_tags( $new_instance['youtube'] ) : '';
		$instance['tumblr']      = ( ! empty( $new_instance['tumblr'] ) ) ? strip_tags( $new_instance['tumblr'] ) : '';

		return $instance;
	}
}

// Register and load the widget
function social_link_load_widget() {
	register_widget( 'Thim_Social_Link_Widget' );
}

add_action( 'widgets_init', 'social_link_load_widget' );