<div class="container">
	<div class="wrapper-logo">
		<div class="tm-table">
			<div class="menu-mobile-effect navbar-toggle" data-effect="mobile-effect">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</div>
		</div>
		<div class="sm-logo hidden-xs">
			<?php do_action( 'thim_logo' ); ?>
		</div>
		<div class="sm-logo sm-logo-mobile visible-xs-block">
			<?php do_action( 'thim_logo_mobile' ); ?>
		</div>
		<div class="sm-logo-affix logo-effect hidden-xs">
			<?php do_action( 'thim_sticky_logo' ); ?>
		</div>
		<div class="sm-logo-affix sm-logo-mobile-affix logo-effect visible-xs-block">
			<?php do_action( 'thim_sticky_logo_mobile' ); ?>
		</div>
	</div>
	<!--end wrapper-logo-->

	<div class="header-right">
		<?php
		// show top header
		get_template_part( 'inc/header/top-header' );
		?>
		<div class="navigation affix-top">
			<div class="tm-table">
				<nav class="table-cell navbar-primary" itemscope itemtype="http://schema.org/SiteNavigationElement">
					<?php get_template_part( 'inc/header/main-menu' ); ?>
				</nav>
				<?php if ( is_active_sidebar( 'toolbar-2' ) ) : ?>
					<?php dynamic_sidebar( 'toolbar-2' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>



