<?php
// disable import demo data in thim-framework
function thim_disable_import_demo() {
	return 'yes';
}

add_filter( 'disable_import_demo', 'thim_disable_import_demo' );

//////////////////////////////////////////////////////////////////
/////////// Import demo data
//////////////////////////////////////////////////////////////////
function fastex_importer_demo_data() {
	define( 'DATA_DEMO_DIR', trailingslashit( get_template_directory() ) . 'inc/admin/data/' );
	define( 'DATA_DEMO_URL', trailingslashit( get_template_directory_uri() ) . 'inc/admin/data/' );

	return array(
		array(
			'import_file_name'             => 'Demo-01',
			'local_import_file'            => DATA_DEMO_DIR . 'demodata.xml',
			'local_import_widget_file'     => DATA_DEMO_DIR . 'widgets.wie',
			'local_import_customizer_file' => DATA_DEMO_DIR . 'customizer-setting.dat',
			'import_preview_image_url'     => DATA_DEMO_URL . 'demo-01.jpg',
		),
		array(
			'import_file_name'             => 'Demo-02',
			'local_import_file'            => DATA_DEMO_DIR . 'demodata.xml',
			'local_import_widget_file'     => DATA_DEMO_DIR . 'widgets.wie',
			'local_import_customizer_file' => DATA_DEMO_DIR . 'customizer-setting.dat',
			'import_preview_image_url'     => DATA_DEMO_URL . 'demo-02.jpg',
		),
		array(
			'import_file_name'             => 'Demo-03',
			'local_import_file'            => DATA_DEMO_DIR . 'demodata.xml',
			'local_import_widget_file'     => DATA_DEMO_DIR . 'widgets.wie',
			'local_import_customizer_file' => DATA_DEMO_DIR . 'customizer-setting.dat',
			'import_preview_image_url'     => DATA_DEMO_URL . 'demo-03.jpg',
		),
		array(
			'import_file_name'             => 'Demo-04',
			'local_import_file'            => DATA_DEMO_DIR . 'demodata.xml',
			'local_import_widget_file'     => DATA_DEMO_DIR . 'widgets.wie',
			'local_import_customizer_file' => DATA_DEMO_DIR . 'customizer-setting-rtl.dat',
			'import_preview_image_url'     => DATA_DEMO_URL . 'demo-04.jpg',
		),
	);
}

add_filter( 'pt-ocdi/import_files', 'fastex_importer_demo_data' );
function fastex_after_import_setup( $selected_import ) {
	// Assign menus to their locations.
	$main_menu  = get_term_by( 'name', 'Primary Menu', 'primary' );
	$secon_menu = get_term_by( 'name', 'Secondary Menu', 'secondary' );
	set_theme_mod( 'nav_menu_locations', array(
		'primary'   => $main_menu->term_id,
		'pecondary' => $secon_menu->term_id,
	) );
	$front_page_id = get_page_by_title( 'Home' );

	$revolution_slider = 'home-copy.zip';
	if ( 'Demo-02' === $selected_import['import_file_name'] ) {
		$revolution_slider = 'slider2.zip';
	}elseif ( 'Demo-04' === $selected_import['import_file_name'] ) {
		$revolution_slider = 'home-rtl.zip';
	}
	if ( class_exists( 'RevSlider' ) ) {
		$slider = new RevSlider();
		$slider->importSliderFromPost( true, true, DATA_DEMO_DIR . "data/revslider/'.$revolution_slider.'" );
	}

	$blog_page_id = get_page_by_title( 'Blog' );
	$shop_id      = get_page_by_title( 'Shop' );
	$cart_id      = get_page_by_title( 'Cart' );
	$checkout_id  = get_page_by_title( 'Checkout' );
	$account_id   = get_page_by_title( 'My account' );

	update_option( 'show_on_front', 'page' );
	update_option( 'permalink_structure', '/%postname%/' );
	update_option( 'page_on_front', $front_page_id->ID );
	update_option( 'page_for_posts', $blog_page_id->ID );
	update_option( 'woocommerce_shop_page_id', $shop_id->ID );
	update_option( 'woocommerce_cart_page_id', $cart_id->ID );
	update_option( 'woocommerce_checkout_page_id', $checkout_id->ID );
	update_option( 'woocommerce_myaccount_page_id', $account_id->ID );
}

add_action( 'pt-ocdi/after_import', 'fastex_after_import_setup' );

function ocdi_plugin_page_setup( $default_settings ) {
	$default_settings['menu_slug'] = 'fastex-onclick-demo-import';

	return $default_settings;
}

add_filter( 'pt-ocdi/plugin_page_setup', 'ocdi_plugin_page_setup' );
add_filter( 'pt-ocdi/disable_pt_branding', 'pt_ocdi_disable_pt_branding' );
function pt_ocdi_disable_pt_branding() {
	return true;
}
