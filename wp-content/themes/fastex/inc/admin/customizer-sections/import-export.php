<?php
$data = $titan->createThimCustomizerSection( array(
	'name'     => __( 'Import/Export Settings', 'fastex' ),
	'desc'     => __( 'You can export then import settings from one theme to another conveniently without any problem.', 'fastex' ),
	'position' => 202,
	'id'       => 'import_export',
	'icon'     => 'dashicons dashicons-admin-generic',
) );

$data->createOption( array(
	'name' => __( 'Import settings', 'fastex' ),
	'id'   => 'import_setting',
	'type' => 'customize-import',
	'desc' => __( 'Click Upload button then choose a JSON file (.json) from your computer to import settings to this theme.', 'fastex' ),
) );

$data->createOption( array(
	'name' => __( 'Export settings', 'fastex' ),
	'id'   => 'export_setting',
	'type' => 'customize-export',
	'desc' => __( 'Simply click Download button to export all your settings to a JSON file (.json).', 'fastex' ),
) );
