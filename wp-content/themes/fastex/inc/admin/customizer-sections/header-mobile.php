<?php

// main menu

$header->addSubSection( array(
	'name'     => __( 'Mobile Menu', 'fastex' ),
	'id'       => 'display_mobile_menu',
	'position' => 15,
) );


$header->createOption( array(
	'name'    => __( 'Background color', 'fastex' ),
	'id'      => 'bg_mobile_menu_color',
	'default' => '#222222',
	'type'    => 'color-opacity'
) );


$header->createOption( array(
	'name'    => __( 'Text color', 'fastex' ),
	'id'      => 'mobile_menu_text_color',
	'default' => '#d8d8d8',
	'type'    => 'color-opacity'
) );

$header->createOption( array(
	'name'    => __( 'Config Logo', 'fastex' ),
	'desc'    => '',
	'id'      => 'config_logo_mobile',
	'options' => array(
		'default_logo' => __( 'Default', 'fastex' ),
		'custom_logo'  => __( 'Custom', 'fastex' )
	),
	'type'    => 'select',
	'default' => 'default_logo'
) );


$header->createOption( array(
	'name'    => __( 'Logo', 'fastex' ),
	'id'      => 'logo_mobile',
	'type'    => 'upload',
	'default' => get_template_directory_uri() . '/images/logo.png',
) );

$header->createOption( array(
	'name'    => __( 'Sticky Logo', 'fastex' ),
	'id'      => 'sticky_logo_mobile',
	'type'    => 'upload',
	'default' => get_template_directory_uri() . '/images/sticky-logo.png',
) );