<?php
/*
 * Creating a logo Options
 */

$logo = $titan->createThemeCustomizerSection( array(
	'name'     => 'title_tagline',
	'position' => 1,
) );


$logo->createOption( array(
	'name'    => __( 'Header logo', 'fastex' ),
	'id'      => 'logo',
	'type'    => 'upload',
	'desc'    => __( 'Upload your logo', 'fastex' ),
	'default' => get_template_directory_uri() . '/images/logo.png',
) );

$logo->createOption( array(
	'name'    => __( 'Sticky logo', 'fastex' ),
	'id'      => 'sticky_logo',
	'type'    => 'upload',
	'desc'    => __( 'Upload your sticky logo', 'fastex' ),
	'default' => get_template_directory_uri() . '/images/sticky-logo.png',
) );

$logo->createOption( array(
	'name'        => __( 'Logo background', 'fastex' ),
	'id'          => 'logo_bg_color',
	'type'        => 'color-opacity',
	'default'     => '#006fb2',
	'livepreview' => '$("#masthead .wrapper-logo").css("background-color", value);'
) );

$logo->createOption( array(
	'name'    => __( 'Max width Logo', 'fastex' ),
	'id'      => 'width_logo',
	'type'    => 'number',
	'default' => '122',
	'max'     => '500',
	'min'     => '0',
	'step'    => '1',
	'desc'    => 'Max width logo (px)'
) );

/**
 * Support favicon for WordPress < 4.3
 */
if ( !function_exists( 'wp_site_icon' ) ) {
	$logo->createOption( array(
		'name'    => __( 'Favicon', 'fastex' ),
		'id'      => 'favicon',
		'type'    => 'upload',
		'desc'    => __( 'Upload your favicon', 'fastex' ),
		'default' => get_template_directory_uri() . '/images/favicon.png',
	) );
}