<?php
$styling = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'Utilities', 'fastex' ),
	'position' => 100,
	'id'       => 'styling'
) );

$styling->createOption( array(
	'name'    => esc_html__( 'RTL Support', 'fastex' ),
	'id'      => 'rtl_support',
	'type'    => 'checkbox',
	'desc'    => 'Enable/Disable',
	'default' => false,
) );
//
//$styling->createOption( array(
//	'name'    => esc_html__( 'Import Demo Data', 'fastex' ),
//	'id'      => 'enable_import_demo',
//	'type'    => 'checkbox',
//	'desc'    => 'Enable/Disable',
//	'default' => true,
//) );