<?php

// header Options
$header->addSubSection( array(
	'name'     => __( 'Sticky Menu', 'fastex' ),
	'id'       => 'display_header_menu',
	'position' => 14,
) );

$header->createOption( array(
	'name'    => __( 'Sticky Menu on scroll', 'fastex' ),
	'desc'    => __( 'Check to enable a fixed header when scrolling, uncheck to disable.', 'fastex' ),
	'id'      => 'header_sticky',
	'type'    => 'checkbox',
	'default' => true
) );