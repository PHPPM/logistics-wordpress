<?php
$custom_css = $titan->createThimCustomizerSection( array(
	'name'     => __( 'Custom CSS', 'fastex' ),
	'position' => 85,
) );

/*
 * Archive Display Settings
 */
$custom_css->createOption( array(
	'id'      => 'custom_css',
	'type'    => 'textarea',
	'desc'    => __( 'Put your additional CSS rules here', 'fastex' ),
	'is_code' => true,
) );
