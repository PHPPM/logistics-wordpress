<?php

$footer->addSubSection( array(
	'name'     => __( 'Style', 'fastex' ),
	'id'       => 'display_footer',
	'position' => 10,
) );

$footer->createOption( array(
	'name' => __( 'Background image', 'fastex' ),
	'id'   => 'footer_background_img',
	'type' => 'upload',
	'desc' => __( 'Upload your background', 'fastex' ),
) );

$footer->createOption( array(
	'name'    => __( 'Text color', 'fastex' ),
	'id'      => 'footer_text_font_color',
	'type'    => 'color-opacity',
	'default' => '#a6a6a6',
) );

$footer->createOption( array(
	'name'        => __( 'Background color', 'fastex' ),
	'id'          => 'footer_bg_color',
	'type'        => 'color-opacity',
	'default'     => '#111111',
	'livepreview' => '$("footer#colophon .footer").css("background-color", value);'
) );
