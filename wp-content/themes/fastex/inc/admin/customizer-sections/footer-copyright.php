<?php
$footer->addSubSection( array(
	'name'     => __( 'Copyright', 'fastex' ),
	'id'       => 'display_copyright',
	'position' => 12,
) );

$footer->createOption( array(
	'name'    => __( 'Back to top', 'fastex' ),
	'id'      => 'show_to_top',
	'type'    => 'checkbox',
	'des'     => 'show or hide back to top',
	'default' => true,
) );

$copy_right = 'http://www.thimpress.com';
$footer->createOption( array(
	'name'        => __( 'Copyright text', 'fastex' ),
	'id'          => 'copyright_text',
	'type'        => 'textarea',
	'default'     => __( 'Designed by ', 'fastex' ) . '<a href="' . esc_url($copy_right) . '">ThimPress.</a>' . __( ' Powered by', 'fastex' ) . 'WordPress.',
	'livepreview' => '$("#powered").html(function(){return "<p>"+ value + "</p>";})'
) );