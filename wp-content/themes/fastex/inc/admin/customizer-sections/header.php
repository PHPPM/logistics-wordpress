<?php

/*
 * Creating a Header Options
 */
$header = $titan->createThimCustomizerSection( array(
	'name'     => __( 'Header', 'fastex' ),
	'position' => 50,
	'id'       => 'display-header',
) );
