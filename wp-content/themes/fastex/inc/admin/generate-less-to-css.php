<?php

/**
 * This class generates custom CSS into static CSS file in uploads folder
 * and enqueue it in the frontend
 *
 * CSS is generated only when theme options is saved (changed)
 * Works with LESS (for unlimited color schemes)
 *
 *
 */
require_once( TP_FRAMEWORK_LIBS_DIR . "less/lessc.inc.php" );

if ( is_multisite() ) {
	if ( ! file_exists( trailingslashit( WP_CONTENT_DIR ) . 'uploads/sites/' . get_current_blog_id() . '/tc_stylesheets' ) ) {
		wp_mkdir_p( trailingslashit( WP_CONTENT_DIR ) . 'uploads/sites/' . get_current_blog_id() . '/tc_stylesheets', 0777, true );
	};
	define( 'FASTEX_UPLOADS_FOLDER', trailingslashit( WP_CONTENT_DIR ) . 'uploads/sites/' . get_current_blog_id() . '/tc_stylesheets/' );
	define( 'FASTEX_UPLOADS_URL', trailingslashit( WP_CONTENT_URL ) . 'uploads/sites/' . get_current_blog_id() . '/tc_stylesheets/' );
} else {
	if ( ! file_exists( trailingslashit( WP_CONTENT_DIR ) . 'uploads/tc_stylesheets' ) ) {
		wp_mkdir_p( trailingslashit( WP_CONTENT_DIR ) . 'uploads/tc_stylesheets', 0777, true );
	}
	if ( ! defined( 'FASTEX_UPLOADS_FOLDER' ) ) {
		define( 'FASTEX_UPLOADS_FOLDER', trailingslashit( WP_CONTENT_DIR ) . 'uploads/tc_stylesheets/' );
	}
	if ( ! defined( 'FASTEX_UPLOADS_URL' ) ) {
		define( 'FASTEX_UPLOADS_URL', trailingslashit( WP_CONTENT_URL ) . 'uploads/tc_stylesheets/' );
	}
}

if ( ! defined( 'FASTEX_FILE_NAME' ) ) {
	define( 'FASTEX_FILE_NAME', 'fastex-options.css' );
}

function customcss() {
	$thim_options = get_theme_mods();

	$custom_css = '';
	if ( isset( $thim_options['thim_user_bg_pattern'] ) && $thim_options['thim_user_bg_pattern'] == '1' ) {
		$custom_css .= ' body{background-image: url("' . $thim_options['thim_bg_pattern'] . '"); }';
	}
	if ( isset( $thim_options['thim_bg_pattern_upload'] ) && $thim_options['thim_bg_pattern_upload'] <> '' ) {
		$bg_body = wp_get_attachment_image_src( $thim_options['thim_bg_pattern_upload'], 'full' );
		$custom_css .= ' body{background-image: url("' . $bg_body[0] . '"); }
						body{
							 background-repeat: ' . $thim_options['thim_bg_repeat'] . ';
							 background-position: ' . $thim_options['thim_bg_position'] . ';
							 background-attachment: ' . $thim_options['thim_bg_attachment'] . ';
							 background-size: ' . $thim_options['thim_bg_size'] . ';
						}
 		';
	}
	/* Footer */
	// Background
	if ( isset( $thim_options['thim_footer_background_img'] ) && $thim_options['thim_footer_background_img'] ) {
		$bg_footer = wp_get_attachment_image_src( $thim_options['thim_footer_background_img'], 'full' );
		$custom_css .= '#footer .footer-sidebars .wrap_content .container{
			background-image: url("' . $bg_footer[0] . '");
			background-repeat: no-repeat;
            background-position: 15px 70%;
 		}';
	}
	$custom_css .= $thim_options['thim_custom_css'];
	return $custom_css;
}

function themeoptions_variation( $data ) {
//	var_dump($data);
	$theme_options = array(
		'thim_logo_bg_color',
		'thim_width_logo',

		'thim_body_bg_color',
		'thim_body_primary_color',
		'thim_body_secondary_color',

		// font body
		'thim_font_body',
		'thim_font_title',
		'thim_font_h1',
		'thim_font_h2',
		'thim_font_h3',
		'thim_font_h4',
		'thim_font_h5',
		'thim_font_h6',

		// footer
		'thim_footer_text_font_color',
		'thim_footer_bg_color',

		// Main menu
		'thim_bg_main_menu_color',
		'thim_main_menu_text_color',
		'thim_main_menu_text_hover_color',

		// Mobile Menu
		'thim_bg_mobile_menu_color',
		'thim_mobile_menu_text_color',

	);

	$config_less = '';
	foreach ( $theme_options AS $key ) {
		$option_data = $data[@$key];
		//data[key] is serialize
		if ( is_serialized( $data[@$key] ) || is_array( $data[@$key] ) ) {
			$config_less .= convert_font_to_variable( $data[@$key], $key );
		} else {
			$config_less .= "@{$key}: {$option_data};\n";
		}
	}
	// Write it down to config.less file
//	$fileout = TP_THEME_DIR . "less/config.less";
//	thim_file_put_contents( $fileout, $config_less );
	return $config_less;
}

function convert_font_to_variable( $data, $tag ) {
	//is_serialized
	$value = '';
	if ( is_serialized( $data ) ) {
		$data = unserialize( $data );
	}
	if ( isset( $data['font-family'] ) ) {
		$value = "@{$tag}_font_family: {$data['font-family']};\n";
	}
	if ( isset( $data['color-opacity'] ) ) {
		$value .= "@{$tag}_color: {$data['color-opacity']};\n";
	}
	if ( isset( $data['font-weight'] ) ) {
		$value .= "@{$tag}_font_weight: {$data['font-weight']};\n";
	}
	if ( isset( $data['font-style'] ) ) {
		$value .= "@{$tag}_font_style: {$data['font-style']};\n";
	}
	if ( isset( $data['text-transform'] ) ) {
		$value .= "@{$tag}_text_transform: {$data['text-transform']};\n";
	}
	if ( isset( $data['font-size'] ) ) {
		$value .= "@{$tag}_font_size: {$data['font-size']};\n";
	}
	if ( isset( $data['line-height'] ) ) {
		$value .= "@{$tag}_line_height: {$data['line-height']};\n";
	}
	return $value;
}

function generate( $fileout ) {
	WP_Filesystem();
	$theme_options_data = $css = '';
	global $wp_filesystem;
	$options = get_theme_mods();

	$compiler = new lessc;
	$compiler->setFormatter( 'compressed' );

	$theme_options_data .= themeoptions_variation( $options );
	$theme_options_data .= $wp_filesystem->get_contents( TP_THEME_DIR . 'less/theme-options.less' );

	$css .= $compiler->compile( $theme_options_data );
	$css .= customcss();
	// Determine whether Multisite support is enabled
	if ( ! $wp_filesystem->put_contents( $fileout, $css, FS_CHMOD_FILE ) ) {
		$wp_filesystem->put_contents( $fileout, $css, LOCK_EX, FS_CHMOD_FILE );
	}
}

global $theme_options_data;
$theme_options_data = get_theme_mods();