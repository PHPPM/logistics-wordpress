<?php

/**
 * thim functions and definitions
 *
 * @package thim
 */
/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

define( 'THIM_VERSION', '1.9' );

if ( ! function_exists( 'thim_setup' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function thim_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on thim, use a find and replace
		 * to change 'fastex' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'fastex', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		// This theme uses wp_nav_menu() in two location.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'fastex' ),
		) );

		register_nav_menus( array(
			'secondary' => __( 'Secondary Menu', 'fastex' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'video',
			'gallery',
		) );

		add_theme_support( "title-tag" );
		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'thim_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_post_type_support( 'page', 'excerpt' );
	}

endif; // thim_setup
add_action( 'after_setup_theme', 'thim_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */

function thim_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'fastex' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Left', 'fastex' ),
		'id'            => 'footer-left',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Middle 1', 'fastex' ),
		'id'            => 'footer-mid-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Middle 2', 'fastex' ),
		'id'            => 'footer-mid-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Right', 'fastex' ),
		'id'            => 'footer-right',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Header Top Sidebar', 'fastex' ),
		'id'            => 'toolbar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Header Right Sidebar', 'fastex' ),
		'id'            => 'toolbar-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Sidebar Shop', 'fastex' ),
		'id'            => 'sidebar-shop',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Widget Navigation', 'fastex' ),
		'id'            => 'widget-navigation',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}

add_action( 'widgets_init', 'thim_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function thim_scripts() {
	global $theme_options_data;
	wp_enqueue_style( 'thim-style', get_stylesheet_uri(), array(), THIM_VERSION );
	if ( is_file( FASTEX_UPLOADS_FOLDER . FASTEX_FILE_NAME ) ) {
		wp_enqueue_style( 'thim-fastex-options', FASTEX_UPLOADS_URL . FASTEX_FILE_NAME, array(), filemtime( FASTEX_UPLOADS_FOLDER . FASTEX_FILE_NAME ) );
	} else {
		wp_enqueue_style( 'thim-fastex-options', get_template_directory_uri() . '/default.css', THIM_VERSION );

	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_single() && get_post_type() == 'product' ) {
		wp_enqueue_script( 'thim-flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array( 'jquery' ), '', false );
	}

	wp_enqueue_script( 'thim-main', get_template_directory_uri() . '/js/main.min.js', array(), '', true );

	wp_enqueue_script( 'thim-custom-script', get_template_directory_uri() . '/js/custom-script.js', array( 'jquery' ), THIM_VERSION, true );

	//Load file rtl.css
	if ( isset( $theme_options_data['thim_rtl_support'] ) && $theme_options_data['thim_rtl_support'] == '1' || is_rtl() ) {
		wp_enqueue_style( 'thim-css-rtl', get_template_directory_uri() . '/rtl.css', array() );
	}

}

add_action( 'wp_enqueue_scripts', 'thim_scripts' );

/**
 * load framework
 */

require_once get_template_directory() . '/framework/tp-framework.php';
// require
require TP_THEME_DIR . 'inc/custom-functions.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require TP_THEME_DIR . 'inc/admin/customize-options.php';

// load widgets
require TP_THEME_DIR . 'inc/widgets/widgets.php';

// tax meta
require TP_THEME_DIR . 'inc/tax-meta.php';

if ( is_admin() && current_user_can( 'manage_options' ) ) {
	require TP_THEME_DIR . 'inc/admin/plugins-require.php';
}

// dislay setting layout
require TP_THEME_DIR . 'inc/wrapper-before-after.php';

// if ( is_plugin_active( 'thim-import-demo/init.php' ) ) {
// 	deactivate_plugins( 'thim-import-demo/init.php' );
// }

// onclick import data demo

require TP_THEME_DIR . 'inc/admin/demo-data-oneclick.php';