<?php
/**
 * Loop Price
 *
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
?>

<div class="product-price">
	<?php if ( $price_html = $product->get_price_html() ) : ?>
		<span class="lable-price"><?php esc_html_e( 'Price:', 'fastex' ); ?></span>
		<span class="price"><?php echo ent2ncr($price_html); ?></span>
	<?php endif; ?>
</div>