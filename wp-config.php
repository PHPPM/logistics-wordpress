<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'logistics_luckas' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'redhat' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define( 'FS_METHOD', 'direct' );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mnF=<UF9hicCZXkF/_1): @uX[cQIS6U|Q.xA3ZT8]$hPjFIl|i-z 3cBhl<wEGK' );
define( 'SECURE_AUTH_KEY',  'NA6(JkcUi/vy5nZ]S+Z|({1$y#7{AhGW5py(b|-^*keE/<Yjyw,d#AN!RWa$@gb`' );
define( 'LOGGED_IN_KEY',    'NibQ4}BK~)ZN2ueByMy*/*.{BsZ&,/g0(&*c.]p{;pU7=(oKr0]r0R/r!-C1Gt;5' );
define( 'NONCE_KEY',        'Op6JUOfHuyl5]jm2nntarih@g)?poIe@~UD88kOPW`nb3LW50hL(Gu:E}e|NCE(E' );
define( 'AUTH_SALT',        'Ja$8,vzuT&BDpEeC]0&9Xd=*3~<U38iTkZ2x>-unwKUVfBG;%PW^p482p:Q(3(0T' );
define( 'SECURE_AUTH_SALT', 'qQfy_g>3<EB|aY~^7bH7{rZiT!:t)b,vyRC4bG9z^x6n~~<>No8bX/4yB6+n.`sg' );
define( 'LOGGED_IN_SALT',   'w{KuM^3{BTWe4ue+>@eG^{Ut2moy&2^,PO8G>7l|IT.?xT{uTv)y0P^tgdUby9!4' );
define( 'NONCE_SALT',       'A2] bJpCX*Q{yLo iZW_l9+rFjS/h<>1$3c+l$Xw8&2F|}z:gbVR#fcn8 x6hpK?' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
